package org.bycloud.test.redis;

import java.util.Set;

import org.bycloud.common.base.RedisPool;
import org.bycloud.common.util.RedisPoolUtils;
import org.junit.Test;

import redis.clients.jedis.Jedis;

public class RedisTest {

    @Test
    public void test() {
        Jedis jedis = new Jedis("111.231.61.188");
        jedis.auth("baoy");
        jedis.set("name", "123");

        Set<String> keys = jedis.keys("*");
        System.out.println(keys);
        jedis.close();
    }


    @Test
    public void testRedisPool() {
        RedisPool.getJedis().flushDB();
    }

    @Test
    public void allkeys() {
        for (String s : RedisPool.getJedis().keys("*")) {
            System.out.println(s);
        }
    }


}
