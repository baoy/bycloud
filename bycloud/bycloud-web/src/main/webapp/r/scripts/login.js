Ext.onReady(function () {

    Ext.QuickTips.init();

    var captchaCode = Ext.create('Ext.Img', {
        xtype: 'image',
        src: '/captcha',
        style: 'cursor:pointer;border : 1px solid #d9d9d9;',
        width: 100,
        listeners: {
            render: function (component) {
                component.getEl().on('click', function (e) {
                    this.dom.src = '/captcha?' + (new Date().getTime());
                });
            }
        }
    });

    var specialKey = function (field, e) {
        if (e.getKey() == e.ENTER) {
            doLogon();
        }
    }

    var doLogon = function () {
        var form = loginForm.getForm();
        if (form.isValid()) {
            form.submit({
                success: function (form, action) {
                    window.location.href = action.result.data;
                },
                failure: function (form, action) {
                    Ext.Msg.alert('Failed', action.result.msg);
                }
            });
        }
    }

    var captcha = Ext.create({
        xtype: 'panel',
        height: 32,
        layout: 'hbox',
        items: [{
            labelWidth: 60,
            xtype: 'textfield',
            value: 'admin',
            fieldLabel: '验证码',
            allowBlank: false,
            flex: 1,
            name: 'captcha',
            emptyText: '请输入验证码',
            listeners: {
                specialKey: specialKey
            }
        }, captchaCode]
    })

    var loginForm = Ext.create('Ext.form.Panel', {
        bodyPadding: 15,
        width: 350,
        url: '/login',
        method : 'POST', 
        layout: 'anchor',
        referenceHolder: true,
        defaults: {
            labelWidth: 60,
            xtype: 'textfield',
            height: 30,
            anchor: '100%',
            allowBlank: false
        },
        items: [{
            fieldLabel: '用户名',
            name: 'username',
            emptyText: '请输入用户名',
            reference: 'username',
            value: 'admin',
            listeners: {
                specialKey: specialKey
            }
        }, {
            fieldLabel: '密 码',
            name: 'password',
            value: 'admin',
            emptyText: '请输入密码',
            listeners: {
                specialKey: specialKey
            }
        }, captcha],
        buttons: [{
            text: '重置',
            handler: function () {
                loginForm.getForm().reset();
            }
        }, {
            text: '登录',
            formBind: true,
            disabled: true,
            handler: function () {
                doLogon();
            }
        }]
    });

    Ext.create('Ext.window.Window', {
        title: 'ByCloud管理系统',
        width: 500,
        closable: false,
        modal: true,
        layout: 'fit',
        items: loginForm,
        listeners: {
            'show': function () {
                var username = loginForm.lookup('username');
                username.focus(false, 100);
            }
        }
    }).show();
});