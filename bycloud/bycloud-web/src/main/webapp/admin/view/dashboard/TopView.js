Ext.define('Baoy.view.dashboard.TopView', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.topview',
    requires: ['Baoy.view.dashboard.TopViewModel'],
    viewModel: 'top-viewmodel',
    controller: 'dashboard-main-view',
    defaults: {
        xtype: 'buttontransparent'
    },
    items: [{
        xtype: 'image',
        bind: { // 数据绑定到MainModel中data的system.iconUrl
            hidden: '{!system.iconUrl}', // 如果system.iconUrl未设置，则此image不显示
            src: '{system.iconUrl}' // 根据system.iconUrl的设置来加载图片
        }
    }, {
        xtype: 'label',
        bind: {
            text: '{system.name}' // text值绑定到system.name
        },
        style: 'font-size : 20px; color:#5fa2dd;font-weight: bold;'
    }, {
        xtype: 'label',
        bind: {
            text: '{system.version}'
        }
    }, '->', {
        text: '主页',
        glyph: 0xf015
    }, {
        text: '帮助',
        glyph: 0xf059
    }, {
        text: '关于',
        glyph: 0xf06a
    }, {
        text: '注销',
        glyph: 0xf011,
        handler: 'onLogout'
    }, '->', '->', {
        xtype: 'label',
        html: '欢迎 [ <font style="font-weight: bold;color:#ff573e;">' + Ext.getLoginUser() + ' </font>]'
    }, {
        text: '系统',
        glyph: 0xf0c9,
        menu: [{
            text: '小说系统',
            glyph : 0xf02d
            // menu: [{
            //     text: '工程项目'
            // }, {
            //     text: '工程标段'
            // }]
        }, {
            text : '秒杀系统'
        }, {
            text : '监控系统'
        }]
    }, {
        text: '设置',
        glyph: 0xf013,
        menu: [{
            text: '修改密码',
            handler: 'onModifyPwd'
        }]
    }]

});
