package org.bycloud.security.service;

import java.util.Set;

import org.bycloud.api.auth.RoleService;
import org.bycloud.common.Pager;
import org.bycloud.common.model.Role;
import org.bycloud.security.respository.RoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("roleService")
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleDao roleDao;


    @Override
    public Pager<Role> findPager(int start, int limit, Object... args) {
        return roleDao.findPager(start, limit, args);
    }


	@Override
	public void save(Role role) {
		roleDao.mergeRole(role);
	}


	@Override
	public Set<String> findResourceIds(String roleId) {
		return roleDao.findResourceIds(roleId);
	}
}
