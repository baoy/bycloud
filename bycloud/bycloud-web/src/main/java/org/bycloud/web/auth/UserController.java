package org.bycloud.web.auth;

import java.util.Set;

import org.bycloud.api.auth.UserService;
import org.bycloud.common.Pager;
import org.bycloud.common.Result;
import org.bycloud.common.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping
	public String index() {
		return "auth/user";
	}

	@ResponseBody
	@RequestMapping("/page")
	public Pager<User> findPager(@RequestParam("start") int start, @RequestParam("limit") int limit) {
		return userService.findPager(start, limit);
	}

	@ResponseBody
	@RequestMapping("/roles")
	public Set<String> findUserRoles(String userId) {
		return userService.findRoleIds(userId);
	}

	@ResponseBody
	@RequestMapping("/save")
	public Result save(User user) {
		userService.save(user);
		return Result.success("操作成功.");
	}
	
	@ResponseBody
	@RequestMapping("/pwd")
	public Result updatePwd(String password) {
//		User currentUser = UserUtils.getCurrentUser();
//		currentUser.setPassword(password); 
//		userService.updatePassword(currentUser);
		return Result.success("操作成功.");
	}
}
