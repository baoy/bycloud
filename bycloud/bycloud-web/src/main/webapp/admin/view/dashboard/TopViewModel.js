Ext.define('Baoy.view.dashboard.TopViewModel', {
			extend : 'Ext.app.ViewModel',

			alias : 'viewmodel.top-viewmodel',

			data : {
				name : 'app',

				// 系统信息
				system : {
					name : Ext.appName,
					version : '5.2014.06.60',
					iconUrl : ''
				},

				// 用户单位信息和用户信息
				user : {
					company : '上海小i公司',
					department : '开发部',
					name : '张三丰'
				},

				// 服务单位和服务人员信息
				service : {
					company : '无锡熙旺公司',
					name : '蒋锋',
					phonenumber : '1320528----',
					qq : '78580822',
					email : 'jfok1972@qq.com',
					copyright : '熙旺公司'
				}
			}
		});