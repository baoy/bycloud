Ext.define('Baoy.controller.system.resource.SystemResourceController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.system-resource-controller',
	
	onItemcontextmenu : function(me, node, item, index, event, eOpts) {
		event.preventDefault();
		var menu = this.getView().menu;
		if (menu && menu.items.length > 0) {
			this.getView().setSelection(node); 
			menu.showAt(event.getXY());
		}
	}
})