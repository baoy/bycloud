package org.bycloud.test.service;

import org.bycloud.api.auth.UserService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class SecucityPasswordTest {

	static ApplicationContext apc;
	static UserService userService;

	@BeforeClass
	public static void beforeClass() throws Exception {
		apc = new ClassPathXmlApplicationContext("spring-config.xml");
		userService = (UserService) apc.getBean("userService");
	}

	@Test
	public void Md5PasswordEncoder() {
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		System.out.println(encoder.encodePassword("123456", "hongxf"));
	}

	@Test
	public void BCryptPasswordEncoder() {
		String password = "admin";
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(9);
		String hashedPassword = passwordEncoder.encode(password);// 随机生成salt 每次生成的密码不一样
		System.out.println(hashedPassword);
	}

	@Test
	public void PasswordMatch() {
		String password = "admin";
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(9);
		boolean r = passwordEncoder.matches(password, "$2a$09$fioamaBNpa.GKPVIimo7EuPLr5ni2nxU0caOZgpWyNSeyV.aFmeG2");
		System.out.println(r);
	}
}
