package org.bycloud.api.auth;

import java.util.Set;

import org.bycloud.common.Pager;
import org.bycloud.common.model.User;

public interface UserService {

    public User findByUsername(String username);

    public Pager<User> findPager(int start, int limit, Object... args);

    public Set<String> findPermissions(String username);
    
    public Set<String> findRoleIds(String userId);

    public Set<String> findRoles(String username);

	public boolean save(User user);
	
	public boolean updatePassword(User user);
}
