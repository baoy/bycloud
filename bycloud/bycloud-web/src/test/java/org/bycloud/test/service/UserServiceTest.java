package org.bycloud.test.service;

import java.util.List;

import org.bycloud.api.auth.PermissionService;
import org.bycloud.api.auth.UserService;
import org.bycloud.common.Pager;
import org.bycloud.common.model.Permission;
import org.bycloud.common.model.User;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserServiceTest {

    static ApplicationContext apc;
    static UserService userService;
    static PermissionService permissionService;

    @BeforeClass
    public static void beforeClass() throws Exception {
        apc = new ClassPathXmlApplicationContext("spring-config.xml");
        userService = (UserService) apc.getBean("userService");
        permissionService = (PermissionService) apc.getBean("permissionService");
    }

    @Test
    public void findByUsername() {
        User username = userService.findByUsername("admin");
        System.out.println(username);
    }

    @Test
    public void findPage() {
        Pager<User> pager = userService.findPager(1, 10);
        System.out.println(pager);
    }
    
    @Test
    public void findPermissionList() {
        List<Permission> list = permissionService.findList();
        System.out.println(list);
    }
}
