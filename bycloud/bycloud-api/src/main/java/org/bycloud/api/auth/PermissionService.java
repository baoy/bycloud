package org.bycloud.api.auth;

import java.util.List;

import org.bycloud.common.TreeNode;
import org.bycloud.common.model.Permission;

public interface PermissionService {

	public List<Permission> findList();
	
	public List<TreeNode> getTree();

}
