package org.bycloud.api.auth;


import java.util.Set;

import org.bycloud.common.Pager;
import org.bycloud.common.model.Role;

public interface RoleService {


    public Pager<Role> findPager(int start, int limit, Object... args);

	public void save(Role role);

	public Set<String> findResourceIds(String roleId);

}
