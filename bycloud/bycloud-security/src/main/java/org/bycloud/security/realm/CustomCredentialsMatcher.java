package org.bycloud.security.realm;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.util.ByteSource;
import org.bycloud.common.Encodes;
import org.bycloud.common.util.EncryptUtils;

import lombok.Setter;

@Setter
public class CustomCredentialsMatcher extends SimpleCredentialsMatcher {

	@Override
	public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
		UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
		String jspPwd = new String(usernamePasswordToken.getPassword());

		SimpleAuthenticationInfo authenticationInfo = (SimpleAuthenticationInfo) info;
		ByteSource salt = authenticationInfo.getCredentialsSalt();

		jspPwd = EncryptUtils.entryptPassword(jspPwd, Encodes.decodeHex(salt.toHex()));
		Object dbPwd = getCredentials(info);
		return equals(jspPwd, dbPwd);
	}

}
