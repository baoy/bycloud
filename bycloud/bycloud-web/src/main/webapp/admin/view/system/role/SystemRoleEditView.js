Ext.define('Baoy.view.system.role.SystemRoleEditView', {
	extend : 'Ext.window.Window',
	alias : 'widget.system-role-edit-view',
	requires : [ 'Baoy.view.system.role.SystemRoleViewModel' ],
	width : 500,
	closeAction : 'method-hide',
	controller : 'system-role-controller',
	viewModel : 'system-role-viewmodel',
	referenceHolder : true,
	bind : {
		title : '{windowTitle}'
	},
	modal : true,
	items : [ {
		xtype : 'form',
		url : 'role/save',
		defaults : {
			labelWidth : 60,
			xtype : 'textfield',
			anchor : '100%',
			allowBlank : false
		},
		bodyPadding : 10,
		items : [ {
			name : 'id',
			allowBlank : true,
			hidden : true
		}, {
			fieldLabel : '角色名称',
			name : 'name',
			emptyText : '请输入角色名称'
		}, {
			fieldLabel : '描述',
			name : 'description',
			xtype : 'textarea',
			emptyText : '请输入描述'
		}, {
			fieldLabel : '权限',
			name : 'resourceIds',
			xtype : 'treepickerux',
			displayField : 'text',
			valueField : 'id',
            checkPropagation : 'down',
			checkboxSelect : true,
			multiSelect : true,
			maxPickerHeight : 300, 
			minPickerHeight : 300,
			selectCascade : true,
			reference : 'resourceTree',
			store : Ext.create('Ext.data.TreeStore', {
				autoLoad : true,
				proxy : {
					type : 'ajax',
					url : 'role/resource'
				}
			})
		} ],
		buttons : [ {
			text : '修改',
			handler : 'onSaveRole'
		}, {
			text : '重置',
			handler : function(btn, e) {
				this.up('form').getForm().reset();
			}
		} ]
	} ],
	listeners : {
		hide : 'onHideRefresh'
	}
})