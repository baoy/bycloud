package org.bycloud.security.service;

import java.util.List;

import org.bycloud.api.auth.PermissionService;
import org.bycloud.common.TreeNode;
import org.bycloud.common.model.Permission;
import org.bycloud.security.respository.PermissionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("permissionService")
public class PermissionServiceImpl extends TreeNodeService<Permission> implements PermissionService {

	@Autowired
	private PermissionDao permissionDao;

	@Override
	public List<Permission> findList() {
		return permissionDao.findList();
	}
	
	public List<TreeNode> getTree() {
		return this.getTree(findList(), true);
	}

	@Override
	protected TreeNode toNode(Permission r, Boolean checked) {
		TreeNode node = new TreeNode();
		node.setId(r.getId());
		node.setText(r.getName());
		node.setPid("0");
		node.setChecked(false);
		return node;
	}

}
