Ext.define('Baoy.view.dashboard.MainView', {
    extend: 'Ext.container.Viewport',
    layout: 'border',
    requires: ['Baoy.controller.dashboard.MainController',
        'Baoy.view.system.SystemMainView',
        'Baoy.view.dashboard.TopView'],
    controller: 'dashboard-main-view',
    defaults: {
        split: true
    },
    items: [{
        region: 'north',
        xtype: 'topview',
        maxHeight: 70,
        height: 50
    }, {
        xtype: 'tabpanel',
        region: 'center',
        plain: true,
        tabPosition: 'left',
        items: new LoginUser().filterAllowed([{
            title: '系统管理',
            glyph: 0xf007,
            authName: 'system:view',
            layout: 'fit',
            items: [{
                xtype: 'system-main-view'
            }]
        }, {
            title: '博客管理',
            authName: 'blog:view',
            glyph: 0xf02d
        }]),
        listeners: {
            tabchange: 'onTabChange'
        }
    }]

})