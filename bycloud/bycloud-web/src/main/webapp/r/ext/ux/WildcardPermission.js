var WildcardPermission = function(wildcardString) {
	this.WILDCARD_TOKEN = "*";
	this.PART_DIVIDER_TOKEN = ":";
	this.SUBPART_DIVIDER_TOKEN = ",";
	this.parts = new Array();
	this.setParts(wildcardString);
}

WildcardPermission.prototype = {

	getParts : function() {
		return this.parts;
	},

	setParts : function(wildcardString) {
		var self = this;
		var wildArray = wildcardString.split(self.PART_DIVIDER_TOKEN);
		wildArray.forEach(function(part, index, array) {
			var subparts = part.split(self.SUBPART_DIVIDER_TOKEN);
			self.parts.push(subparts);
		});
	},

	implies : function(p) {
		var i = 0;
		var ret = true, self = this;
		var otherParts = p.getParts();
		otherParts.forEach(function(otherPart, index, array) {
			if (self.getParts().length - 1 < i) {
				return true;
			} else {
				var part = self.getParts()[i];
				if (!part.contains(self.WILDCARD_TOKEN) && !part.contains(otherPart)) {
					ret = false;
				}
				i++;
			}
		})
		return ret;
	}
}