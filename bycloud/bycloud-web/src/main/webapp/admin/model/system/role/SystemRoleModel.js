Ext.define('Baoy.model.system.role.SystemRoleModel', {
			extend: 'Ext.data.Model',
			fields : [ 'id', 'role', 'description', 'available' ]
		});
