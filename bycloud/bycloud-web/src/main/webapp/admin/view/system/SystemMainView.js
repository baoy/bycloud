Ext.define('Baoy.view.system.SystemMainView', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.system-main-view',
	requires : [ 'Baoy.controller.system.SystemMainController',
			'Baoy.view.system.user.SystemUserView',
			'Baoy.view.system.role.SystemRoleView',
			'Baoy.view.system.resource.SystemResourceView',
			'Baoy.view.system.permission.SystemPermissionView' ],
	controller : 'system-main-controller',
	layout : 'border',
	bodyBorder : false,
	defaults : {
		split : true
	},
	items : [ {
		region : 'west',
		width : 200,
		xtype : 'treepanel',
		rootVisible : false,
		border : false,
		lines : true,
		title : '导航',
		store : {
			xtype : 'treestore',
			root : {
				id : '0',
				expanded : true,
				children : new LoginUser().filterAllowed([ {
					id : 'user',
					glyph : 0xf007,
					authName : 'user:view',
					text : '用户管理',
					leaf : true
				}, {
					id : 'role',
					glyph : 0xf0f0,
					authName : 'role:view',
					text : '角色管理',
					leaf : true
				}, {
					id : 'resource',
					glyph : 0xf039,
					authName : 'resource:view',
					text : '菜单管理',
					leaf : true
				}, {
					id : 'permission',
					glyph : 0xf0ad,
					authName : 'permision:view',
					text : '权限管理',
					leaf : true
				} ])
			}
		},
		listeners : {
			itemclick : 'onSystemClick',
			afterrender : 'onOpenFirstNode'
		}
	}, {
		xtype : 'tabpanel',
		reference : 'system-tab-view',
		defaults : {
			closable : true,
			split : true
		},
		region : 'center',
		listeners : {
			tabchange : 'onTabChange'
		}
	} ]
})