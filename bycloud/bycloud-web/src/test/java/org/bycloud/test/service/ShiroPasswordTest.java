package org.bycloud.test.service;


import org.bycloud.common.Digests;
import org.bycloud.common.Encodes;
import org.bycloud.common.util.EncryptUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShiroPasswordTest {
	
	static ApplicationContext apc = null;
	
	@Before
	public void before() {
		apc = new ClassPathXmlApplicationContext("spring-config.xml","spring-shiro.xml");
	}
	
	@Test
	public void testPwd() {
		System.out.println(EncryptUtils.entryptPassword("admin", EncryptUtils.generateSalt()));
	}

    public static final String HASH_ALGORITHM = "SHA-1";
    public static final int HASH_INTERATIONS = 1024;
    public static final int SALT_SIZE = 8;

    public static String entryptPassword(String plainPassword) {
        String plain = Encodes.unescapeHtml(plainPassword);
        byte[] salt = Digests.generateSalt(SALT_SIZE);
        byte[] hashPassword = Digests.sha1(plain.getBytes(), salt, HASH_INTERATIONS);
        if (log.isDebugEnabled()) {
            log.debug("Digests generateSalt is : {}, generatePassword is : {}", Encodes.encodeHex(salt), Encodes.encodeHex(hashPassword));
        }
        return Encodes.encodeHex(salt) + Encodes.encodeHex(hashPassword);
    }

    @Test
    public void testPassword() {
        System.out.println(entryptPassword("admin"));
    }

//    @Test
//    public void validPassword() {
//        String plainPassword = "02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032";
//        byte[] salt = Encodes.decodeHex(plainPassword.substring(0,16));
//        String realPassword = plainPassword.substring(16);
//        System.out.println();
//    }
}
