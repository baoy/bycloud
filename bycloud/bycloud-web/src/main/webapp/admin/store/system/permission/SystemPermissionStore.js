Ext.define('Baoy.store.system.permission.SystemPermissionStore', {
	extend : 'Ext.data.Store',
	alias : 'store.system-permission-store',
	model : 'Baoy.model.system.permission.SystemPermissionModel',
	autoLoad : true,
	idProperty : 'id',
	proxy : {
		type : 'ajax',
		url : 'permission/list',
		reader : {
			type : 'json',
			rootProperty : ''
		}
	}
});
