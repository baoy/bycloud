Ext.define('Baoy.view.system.permission.SystemPermissionView', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.system-permission-view',
	requires : [ 'Baoy.store.system.permission.SystemPermissionStore',
			'Baoy.controller.system.permission.SystemPermissionController' ],
	controller : 'system-permission-controller',
	store : {
		type : 'system-permission-store'
	},
	columnLines : true,
	columns : [ {
		xtype : 'rownumberer'
	}, {
		text : '权限',
		flex : 1,
		dataIndex : 'name',
		editor : {
			allowBlank : false
		}
	}, {
		text : '代码',
		flex : 1,
		dataIndex : 'code',
		editor : {
			allowBlank : false
		}
	}, {
		text : '描述',
		flex : 1,
		dataIndex : 'description',
		editor : {
			allowBlank : true
		}
	} ],
	tbar : [ {
		text : '添加',
		glyph : 0xf0fe,
		tooltip : '添加',
		handler : 'onAddPermission'
	}, {
		text : '保存',
		glyph : 0xf044,
		tooltip : '修改',
		handler : 'onEditPermission'
	}, {
		text : '删除',
		glyph : 0xf057,
		tooltip : '删除',
		handler : 'onRemovePermission'
	} ],
	plugins : {
		ptype : 'cellediting',
		clicksToEdit : 2
	}
})