package org.bycloud.web.auth;

import java.util.List;
import java.util.Set;

import org.bycloud.api.auth.ResourceService;
import org.bycloud.api.auth.RoleService;
import org.bycloud.common.Pager;
import org.bycloud.common.Result;
import org.bycloud.common.TreeNode;
import org.bycloud.common.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/role")
public class RoleController {

	@Autowired
	RoleService roleService;
	@Autowired
	ResourceService resourceService;

	@ResponseBody
	@RequestMapping("/page")
	public Pager<Role> findPager(@RequestParam("start") int start, @RequestParam("limit") int limit) {
		return roleService.findPager(start, limit);
	}

	@ResponseBody
	@RequestMapping("/save")
	public Result save(Role role) {
		roleService.save(role);
		return Result.success("操作成功.");
	}

	@ResponseBody
	@RequestMapping("/resource")
	public List<TreeNode> getRoleResourceTree() {
		return resourceService.getTree();
	}

	@ResponseBody
	@RequestMapping("/resources")
	public Set<String> findRoleResources(String roleId) {
		return roleService.findResourceIds(roleId);
	}

}
