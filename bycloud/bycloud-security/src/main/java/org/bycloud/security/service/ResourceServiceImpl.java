package org.bycloud.security.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.bycloud.api.auth.ResourceService;
import org.bycloud.common.TreeNode;
import org.bycloud.common.model.Resource;
import org.bycloud.security.respository.ResourceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("resourceService")
public class ResourceServiceImpl extends TreeNodeService<Resource> implements ResourceService {

	@Autowired
	ResourceDao resourceDao;

	public List<TreeNode> getTree() {
		List<Resource> resources = resourceDao.findAll();
		List<Resource> btns = new ArrayList<Resource>();
		for (Resource resource : resources) {
			// 加载菜单下面所有的按钮操作
			List<Resource> childBtns = resourceDao.findByResourceId(resource.getId());
			if (CollectionUtils.isNotEmpty(childBtns)) {
				btns.addAll(childBtns);
			}
		}
		resources.addAll(btns);
		return this.getTree(resources, true);
	}

	public List<Resource> findMenus(Set<String> permissions) {
		List<Resource> resources = resourceDao.findMenus();
		List<Resource> menus = new ArrayList<Resource>();
		for (Resource resource : resources) {
			if (resource.isRootNode()) {
				continue;
			}
			if (StringUtils.equals(resource.getType(), Resource.ResourceType.menu.name())) {
				continue;
			}
			if (!hasPermission(permissions, resource)) {
				continue;
			}
			menus.add(resource);
		}
		return menus;
	}

	private boolean hasPermission(Set<String> permissions, Resource resource) {
		if (StringUtils.isEmpty(resource.getPermission())) {
			return true;
		}
		for (String permission : permissions) {
			WildcardPermission p1 = new WildcardPermission(permission);
			WildcardPermission p2 = new WildcardPermission(resource.getPermission());
			if (p1.implies(p2) || p2.implies(p1)) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected TreeNode toNode(Resource r, Boolean checked) {
		TreeNode node = new TreeNode();
		node.setId(r.getId());
		node.setText(r.getName());
		node.setPid(r.getParentId());
		node.setAttachment(r);
		if (checked)
			node.setChecked(false);
		return node;
	}

	@Override
	public List<Resource> findChildren(String node) {
		List<Resource> childrenResources = resourceDao.findChildren(node);
		// 查找 menu 下面的 button 需要查找 t_resouce_permission
		List<Resource> btns = resourceDao.findByResourceId(node);
		if (CollectionUtils.isNotEmpty(btns)) {
			childrenResources.addAll(btns);
		}
		return childrenResources;
	}

	@Override
	public List<TreeNode> findTreeNodes(String node, Boolean checked) {
		List<Resource> res = this.findChildren(node);
		return toNodes(res, checked);
	}

	@Override
	public List<Resource> findAll() {
		return resourceDao.findAll();
	}

	@Override
	@Transactional(readOnly = false)
	public boolean save(Resource resource) {
		return resourceDao.mergeResource(resource);
	}

	@Override
	public Set<String> findPermissionIds(String resourceId) {
		return resourceDao.findPermissionIds(resourceId);
	}
}
