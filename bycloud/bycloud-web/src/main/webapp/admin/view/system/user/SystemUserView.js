Ext.define('Baoy.view.system.user.SystemUserView', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.system-user-view',
	requires : [ 'Baoy.model.system.user.SystemUserModel',
			'Baoy.store.system.user.SystemUserStore',
			'Baoy.controller.system.user.SystemUserController',
			'Baoy.view.system.user.SystemUserEditView' ],
	controller : 'system-user-controller',
	store : {
		type : 'system-user-store'
	},
	columnLines : true,
	columns : [ {
		xtype : 'rownumberer'
	}, {
		text : '用户名',
		flex : 1,
		dataIndex : 'username'
	}, {
		text : '昵称',
		flex : 1,
		dataIndex : 'nickname'
	}, {
		text : '手机',
		flex : 1,
		dataIndex : 'phone'
	}, {
		text : '性别',
		dataIndex : 'sex',
		renderer : function(value, record) {
			return value === 1 ? '男' : '女';
		}
	}, {
		text : '创建时间',
		dataIndex : 'createtime',
		flex : 1,
		renderer : function(value, record) {
			return value ? Ext.util.Format.date(value, "Y-m-d H:i:s") : '';
		}
	} ],
	tbar : new LoginUser().filterAllowed([{
		text : '添加',
		glyph : 0xf0fe,
		authName : 'user:add',
		tooltip : '添加',
		handler : 'onAddUser'
	}, {
		text : '修改',
		glyph : 0xf044,
		authName : 'user:edit',
		tooltip : '修改',
		handler : 'onEditUser'
	}, {
		text : '删除',
		glyph : 0xf057,
		authName : 'user:delete',
		tooltip : '删除',
		handler : 'onRemoveUser'
	} ]),
	bbar : {
		xtype : 'pagingtoolbar',
		displayInfo : true
	},
	listeners : {
		rowdblclick : 'onRowDblClick'
	}
})