Ext.define('Baoy.view.system.role.SystemRoleView', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.system-role-view',
	requires : [ 'Baoy.store.system.role.SystemRoleStore',
			'Baoy.controller.system.role.SystemRoleController' ],
	controller : 'system-role-controller',
	store : {
		type : 'system-role-store'
	},
	columnLines : true,
	columns : [ {
		xtype : 'rownumberer'
	}, {
		text : '角色',
		width : 120,
		dataIndex : 'name'
	}, {
		text : '描述',
		flex : 1,
		dataIndex : 'description'
	}, {
		text : '是否可用',
		width : 100,
		xtype : 'checkcolumn',
		dataIndex : 'available',
		listeners : {
			checkchange : 'onChangeAvailable'
		}
	} ],
	tbar : new LoginUser().filterAllowed([ {
		text : '添加',
		glyph : 0xf0fe,
		tooltip : '添加',
		authName : 'role:add',
		handler : 'onAddRole'
	}, {
		text : '修改',
		glyph : 0xf044,
		tooltip : '修改',
		authName : 'role:edit',
		handler : 'onEditRole'
	}, {
		text : '删除',
		glyph : 0xf057,
		tooltip : '删除',
		authName : 'role:delete',
		handler : 'onRemoveRole'
	} ]),
	bbar : {
		xtype : 'pagingtoolbar',
		displayInfo : true
	},
	listeners : {
		rowdblclick : 'onRowDblClick'
	}
})