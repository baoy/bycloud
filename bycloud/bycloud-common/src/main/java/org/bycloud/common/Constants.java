package org.bycloud.common;

public class Constants {

    public static final String COLONS = ":";

	public static final String CURRENT_USER = "currentUser";
	
    public static final String CAPTCHA = "captcha";
    
    public static final String INPARAMS = "inArgs";
    
    public interface RedisCacheExtime{
        int REDIS_SESSION_EXTIME = 60 * 30;//30分钟
    }
    
}
