package org.bycloud.common;

import java.io.Serializable;
import java.util.List;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TreeNode implements Serializable {
    private static final long serialVersionUID = -8605738255033023107L;
    public String id;
    public String text;
    public String pid;
    public Boolean leaf;
    public String icon;
    public String url;
    public Boolean checked;
    public List<TreeNode> children;
    public Object attachment; 

    public TreeNode(String id, String text, String pid, Boolean leaf, String url, Object attachment) {
        this.id = id;
        this.text = text;
        this.pid = pid;
        this.leaf = leaf;
        this.url = url;
        this.attachment = attachment;
    }
}
