package org.bycloud.security.respository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bycloud.common.base.SimpleJdbcDao;
import org.bycloud.common.model.Resource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ResourceDao extends SimpleJdbcDao<Resource> {

	static String insertSql = "insert into t_resource (id,name,type,url,parent_id,parent_ids,permission,available,icon) values (:id,:name,:type,:url,:parent_id,:parent_ids,:permission,:available,:icon)";
	static String updateSql = "update t_resource set name = :name,type = :type,url = :url,parent_id = :parentId,parent_ids = :parentIds,permission = :permission,available = :available where id = :id ";
	static String insertResPerSql = "insert into t_resource_permission (resource_id,permission_id) values (?,?)";
	static String deleteResPerSql = "delete from t_resource_permission where resource_id = ?";
	private static String findByResourceIdSql = "select concat(rp.resource_id,'_', rp.permission_id) as id, p.name as name,rp.resource_id as parentId, rp.resource_id as resourceId, rp.permission_id as permissionId, 'true' as leaf from t_permission p left join t_resource_permission rp on rp.permission_id = p.id where rp.resource_id = ?";
	private static String findByIdSql = "select * from t_resource where resource_id = ?";
	private static String findChildren = "select * from t_resource where parent_id = ?";
	static String findResourceIdsSql = "select rr.permission_id from t_resource_permission rr where rr.resource_id = ?";
	
	public Resource findOne(String resourceId) {
		return findModel(findByIdSql, resourceId);
	}

	public List<Resource> findChildren(String resourceId) {
		return findModels(findChildren, resourceId);
	}

	public List<Resource> findAll() {
		return findModels("select * from t_resource", NULL_PARA_ARRAY);
	}

	public List<Resource> findMenus() {
		return findAll();
	}

	public List<Resource> findByResourceId(String resourceId) {
		return super.jdbcTemplate.query(findByResourceIdSql, new BeanPropertyRowMapper<Resource>(Resource.class),
				new Object[] { resourceId });
	}

	public boolean saveResource(Resource resource) {
		String resourceId = getId();
		if (StringUtils.isBlank(resource.getParentId())) {
			resource.setParentId("0");
		}
		resource.setId(resourceId);
		saveModel(insertSql, resource);// save resource
		List<String> permissionIds = resource.getPermissionIds();
		if (CollectionUtils.isNotEmpty(permissionIds)) { // save resource-permission
			for (String permissionId : permissionIds) {
				jdbcTemplate.update(insertResPerSql, new Object[] { resource.getId(), permissionId });
			}
		}
		return true;
	}

	public boolean updateResource(Resource resource) {
		if (StringUtils.isBlank(resource.getParentId())) {
			resource.setParentId("0");
		}
		saveModel(updateSql, resource); // update resource
		List<String> permissionIds = resource.getPermissionIds();
		jdbcTemplate.update(deleteResPerSql, new Object[] { resource.getId() });
		if (CollectionUtils.isNotEmpty(permissionIds)) { // save resource-permission
			for (String permissionId : permissionIds) {
				jdbcTemplate.update(insertResPerSql, new Object[] { resource.getId(), permissionId });
			}
		}
		return true;
	}

	public boolean mergeResource(Resource resource) {
		return StringUtils.isBlank(resource.getId()) ? saveResource(resource) : updateResource(resource);
	}

	public Set<String> findPermissionIds(String resourceId) {
		List<String> resourceIds = super.findStrings(findResourceIdsSql, resourceId);
		return resourceIds != null && resourceIds.size() > 0 ? new HashSet<String>(resourceIds) : this.emptySet;
	}

}
