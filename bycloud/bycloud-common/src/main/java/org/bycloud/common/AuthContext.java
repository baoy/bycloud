package org.bycloud.common;

import java.util.Collection;

import org.bycloud.common.model.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthContext {

	private Collection<String> permissions;
	private User user;

	static ThreadLocal<AuthContext> threadLocal = new ThreadLocal<AuthContext>();

	public static AuthContext createSysUserContext() {
		AuthContext context = new AuthContext();
		return context;
	}

	public static AuthContext getContext(boolean supressException) {
		AuthContext context = threadLocal.get();
		if (context == null && !supressException)
			throw new IllegalStateException("no authcontext available - maybe not in the same thread as web actions");
		return context;
	}

	public static AuthContext getContext() {
		return getContext(false);
	}

	public static void remove() {
		threadLocal.remove();
	}

	public static void set(AuthContext authContext) {
		if (authContext == null)
			threadLocal.remove();
		else
			threadLocal.set(authContext);
	}
}
