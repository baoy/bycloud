package org.bycloud.security.realm;

import java.text.MessageFormat;
import java.util.Collection;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.alibaba.fastjson.JSONObject;

public class CheckPermTag extends TagSupport {

    private static final long serialVersionUID = 8889141716110943775L;
    static final MessageFormat TPL_SCRIPT = new MessageFormat("<script>var check_per_ret={0}; var user_login={1};</script>");

    public int doEndTag() throws JspException {
        Collection<String> pers = UserUtils.getUserPermission();
        try {
            pageContext.getOut().write(TPL_SCRIPT.format(new Object[]{JSONObject.toJSON(pers), JSONObject.toJSONString(UserUtils.getUsername())}));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return EVAL_PAGE;
    }

}
