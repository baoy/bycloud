Ext.define('Baoy.store.system.user.SystemUserStore', {
	extend : 'Ext.data.Store',
	alias : 'store.system-user-store',
	model : 'Baoy.model.system.user.SystemUserModel',
	autoLoad : true,
	idProperty : 'id',
	proxy : {
		type : 'ajax',
		url : 'user/page',
		reader : {
			type : 'json',
			rootProperty : 'data',
			totalProperty : 'total'
		}
	}
});
