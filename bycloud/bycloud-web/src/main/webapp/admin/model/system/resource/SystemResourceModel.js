Ext.define('Baoy.model.system.resource.SystemResourceModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'string'
					}, {
						name : 'name',
						type : 'string'
					}, {
						name : 'type',
						type : 'string'
					}, {
						name : 'parentId',
						type : 'string'
					}, {
						name : 'parentIds',
						type : 'string'
					}, {
						name : 'available',
						type : 'boolean'
					}, {
						name : 'done',
						type : 'boolean'
					}]
		});
