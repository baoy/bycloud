Ext.define('Baoy.controller.system.permission.SystemPermissionController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.system-permission-controller',

	onAddPermission : function(btn, e) {
		var grid = this.getView();
		grid.store.insert(0, rec);
		grid.findPlugin('cellediting').startEdit(rec, 0);
	},

	onEditPermission : function(btn, e) {
		var grid = this.getView();
		var selectRows = grid.getSelection();
		if (selectRows && selectRows.length > 0) {
			selectRow = selectRows[0];
			if (!grid.editPermissionWin) {
				var editPermissionWin = Ext.create('Baoy.view.system.permission.SystemPermissionEditView');
				editPermissionWin.parentGrid = grid;
				grid.editPermissionWin = editPermissionWin;
			}
			var roles = http_get('permission/roles?permissionId=' + selectRow.data.id);
			if (roles) {
				var rs = Ext.decode(roles);
				selectRow.data.roleIds = rs;
			}
			grid.editPermissionWin.getViewModel().set('windowTitle', '修改用户');
			var dataForm = grid.editPermissionWin.down('form').getForm();
			dataForm.reset();
			dataForm.loadRecord(selectRow);
			grid.editPermissionWin.show();
		}
	},

	onRemovePermission : function(btn, e) {
		Ext.MessageBox.confirm('提示', '确认删除？', function(optional) {
			if (optional == 'yes') {
				var grid = this.getView();
				grid.getStore().remove(grid.getSelection());
				grid.getView().refresh();
			}
		}, this);
	},

	onRowDblClick : function(me, record, element, rowIndex, e, eOpts) {
		this.onEditPermission()
	},

	onSavePermission : function(btn, e) {
		var self = this;
		var form = this.getView().down('form').getForm();
		if (form.isValid()) {
			form.submit({
				success : function(form, action) {
					if (action.result.success) {
						self.getView().hide();
					}
					Ext.info(action.result.msg);
				},
				failure : function(form, action) {
					Ext.Msg.alert('Failed', action.result.msg);
				}
			});
		}
	},

	onHideRefresh : function() {
		var editWin = this.getView();
		editWin.parentGrid.getStore().reload();
	}
})