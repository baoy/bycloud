Ext.define('Baoy.view.system.resource.SystemResourceView', {
	extend : 'Ext.tree.Panel',
	alias : 'widget.system-resource-view',
	requires : [ 'Baoy.model.system.resource.SystemResourceModel',
			'Baoy.controller.system.resource.SystemResourceController' ],
	controller : 'system-resource-controller',
	store : {
		type : 'tree',
		model : 'Baoy.model.system.resource.SystemResourceModel',
		folderSort : true,
		proxy : {
			type : 'ajax',
			url : 'resource/tree'
		}
	},
	reserveScrollbar : true,
	useArrows : true,
	rootVisible : false,
	multiSelect : true,
	singleExpand : true,
	columnLines : true,
	rowLines : true,
	columns : [ {
		xtype : 'treecolumn',
		text : '名称',
		dataIndex : 'name',
		flex : 2,
		sortable : true
	}, {
		text : '类型',
		dataIndex : 'type',
		flex : 1
	}, {
		text : '路径',
		dataIndex : 'url',
		flex : 1,
		sortable : true
	}, {
		text : '权限',
		dataIndex : 'permission',
		flex : 1,
		sortable : true
	}
	/*, {
		xtype : 'checkcolumn',
		header : '是否启用',
		dataIndex : 'available',
		width : 85,
		stopSelection : false,
		menuDisabled : true
	}, {
		xtype : 'actioncolumn',
		text : 'Edit',
		width : 55,
		menuDisabled : true,
		tooltip : 'Edit task',
		align : 'center',
		iconCls : 'tree-grid-edit-task'
	}*/ 
	],
	initComponent : function() {
		var me = this;
		this.menu = Ext.create('Ext.menu.Menu', {
			items : new LoginUser().filterAllowed([ {
				text : "新增",
				//authName : 'resource:add',
				glyph : 0xf0fe,
				handler : function() {
					me.resourceForm.reset();
					me.resourceWindow.show();
				}
			}, {
				text : "修改",
				//authName : 'resource:edit',
				glyph : 0xf044,
				handler : function() {
					var nodes = me.getSelection();
					if (nodes.length > 0) {
						var permissionTree = me.resourceForm.lookup('permissionTree');
						var permissions = http_get('resource/permissions?resourceId=' + nodes[0].data.id);
						if (permissions) {
							var rs = Ext.decode(permissions);
							if (rs && rs.length > 0) {
								var permissionIds = [];
								Ext.each(rs, function(r) { permissionIds.push({ id : r }); });
								Ext.defer(function() {
									permissionTree.setValue(permissionIds);
								}, 500, this);
							}
						}
						
						me.resourceForm.reset();
						me.resourceForm.getForm().setValues(nodes[0].data);
					}
					me.resourceWindow.show();
				}
			}, {
				text : "删除",
				//authName : 'resource:remove',
				glyph : 0xf2ed,
				handler : function() {

				}
			} ])
		});

		this.resourceForm = Ext.create('Ext.form.Panel', {
			defaults : {
				anchor : '100%',
				labelWidth : 58
			},
			url : 'resource/save',
			bodyPadding : 10,
			referenceHolder : true,
			defaultType : 'textfield',
			items : [ {
				name : 'id',
				hidden : true
			}, {
				name : 'parentId',
				hidden : true
			}, {
				name : 'name',
				fieldLabel : '名称',
				allowBlank : false
			}, {
				name : 'permission',
				fieldLabel : '权限',
				allowBlank : false
			}, {
				name : 'url',
				fieldLabel : '路径',
				allowBlank : false
			}, {
				fieldLabel : '权限',
				name : 'permissionIds',
				xtype : 'treepickerux',
				displayField : 'text',
				valueField : 'id',
				checkboxSelect : true,
				multiSelect : true,
				maxPickerHeight : 300,
				minPickerHeight : 300,
				selectCascade : true,
				rootVisible : true,
				checkPropagation : 'both',
				reference : 'permissionTree',
				store : Ext.create('Ext.data.TreeStore', {
					autoLoad : true,
					root : {
						id : '0',
						text : '全选',
						checked : false,
						expanded : true
					},
					proxy : {
						type : 'ajax',
						url : 'permission/tree'
					}
				})
			} ]
		});

		this.resourceWindow = Ext.create('Ext.window.Window', {
			layout : 'fit',
			title : '编辑资源',
			closeAction : 'hide',
			width : 450,
			modal : true,
			border : false,
			items : me.resourceForm,
			buttons : [ {
				text : '保存',
				handler : function() {
					var form = me.resourceForm.getForm();
					if (form.isValid()) {
						form.submit({
							success : function(form, action) {
								if (action.result.success) {
									me.resourceWindow.hide();
								}
								Ext.info(action.result.msg);
							},
							failure : function(form, action) {
								Ext.error(action.result.msg);
							}
						});
					}
				}
			}, {
				text : '重置',
				handler : function() {
					me.resourceForm.reset();
				}
			} ]
		});
		this.callParent();
	},
	listeners : {
		itemcontextmenu : 'onItemcontextmenu'
	}
})