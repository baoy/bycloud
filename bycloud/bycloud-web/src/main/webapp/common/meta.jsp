<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
<script charset="UTF-8" src="<c:url value='/r/ext/ext-all-debug.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/r/ext/locale-zh_CN.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/r/ext/theme-triton/theme-triton.js'/>"></script>
<link   charset="UTF-8" rel="stylesheet" type="text/css" href="<c:url value='/r/ext/theme-triton/theme-triton-all.css'/>" />
<link   charset="UTF-8" rel="stylesheet" type="text/css" href="<c:url value='/r/ext/css/font-awesome.css'/>" />



<!-- override  修复extjs的bug方法 -->
<script charset="UTF-8" src="<c:url value='/r/ext/ux/WildcardPermission.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/r/ext/ux/TreePickerEx.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/r/ext/ux/Renderer.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/r/ext/ux/commons.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/r/ext/ux/override-ux.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/r/ext/ux/ButtonTransparent.js'/>"></script>
