package org.bycloud.security.respository;

import java.util.List;

import org.bycloud.common.base.SimpleJdbcDao;
import org.bycloud.common.model.Permission;
import org.springframework.stereotype.Repository;

@Repository
public class PermissionDao extends SimpleJdbcDao<Permission> {

	private static String allSql = "select * from t_permission";
	

	public List<Permission> findList() {
		return super.findModels(allSql);
	}

}
