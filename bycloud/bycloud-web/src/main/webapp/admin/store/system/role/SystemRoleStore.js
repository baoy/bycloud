Ext.define('Baoy.store.system.role.SystemRoleStore', {
			extend : 'Ext.data.Store',
			alias : 'store.system-role-store',
			model : 'Baoy.model.system.role.SystemRoleModel',
			autoLoad : true,
			idProperty : 'id',
			proxy : {
				type : 'ajax',
				url : 'role/page',
				reader : {
					type : 'json',
					rootProperty : 'data',
					totalProperty : 'total'
				}
			}
		});
