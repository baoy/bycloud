Ext.define('Baoy.model.system.permission.SystemPermissionModel', {
			extend: 'Ext.data.Model',
			fields : [ 'id', 'name', 'code', 'description' ]
		});
