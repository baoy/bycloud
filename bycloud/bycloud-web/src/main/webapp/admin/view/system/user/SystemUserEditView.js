Ext.define('Baoy.view.system.user.SystemUserEditView', {
	extend : 'Ext.window.Window',
	alias : 'widget.system-user-edit-view',
	requires : [ 'Baoy.view.system.user.SystemUserViewModel',
			'Baoy.store.system.role.SystemRoleStore' ],
	width : 500,
	controller : 'system-user-controller',
	referenceHolder : true,
	closeAction : 'method-hide',
	viewModel : 'system-user-viewmodel',
	bind : {
		title : '{windowTitle}'
	},
	modal : true,
	items : [ {
		xtype : 'form',
		url : 'user/save',
		defaults : {
			labelWidth : 60,
			xtype : 'textfield',
			anchor : '100%',
			allowBlank : false
		},
		bodyPadding : 10,
		items : [ {
			name : 'id',
			allowBlank : true,
			hidden : true
		}, {
			fieldLabel : '用户名',
			name : 'username',
			emptyText : '请输入用户名'
		}, {
			fieldLabel : '昵称',
			name : 'nickname',
			emptyText : '请输入昵称'
		}, {
			fieldLabel : '手机号码',
			name : 'phone',
			emptyText : '请输入手机'
		}, {
			fieldLabel : '性别',
			xtype : 'combobox',
			name : 'sex',
			store : {
				fields : [ 'id', 'text' ],
				data : [ {
					"id" : "1",
					"text" : "男"
				}, {
					"id" : "0",
					"text" : "女"
				} ]
			},
			valueField : 'id',
			displayField : 'text',
			typeAhead : true,
			queryMode : 'local',
			emptyText : '选择性别'
		}, {
			xtype : 'datefield',
			fieldLabel : '修改日期',
			name : 'createtime',
			readOnly : true,
			allowBlank : true,
			editable : false,
			format : 'Y-m-d H:i:s'
		}, {
			xtype : 'tagfield',
			fieldLabel : '选择角色',
			store : {
				type : 'system-role-store'
			},
			displayField : 'name',
			valueField : 'id',
			name : 'roleIds',
			createNewOnEnter : true,
			createNewOnBlur : true,
			filterPickList : true,
			queryMode : 'local',
			publishes : 'value'
		} ],
		buttons : [ {
			text : '保存',
			handler : 'onSaveUser'
		}, {
			text : '重置',
			handler : function(btn, e) {
				this.up('form').getForm().reset();
			}
		} ]
	} ],
	listeners : {
		hide : 'onHideRefresh'
	}
})