package org.bycloud.web.auth;

import java.util.List;

import org.bycloud.api.auth.PermissionService;
import org.bycloud.common.TreeNode;
import org.bycloud.common.model.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/permission")
public class PermissionController {

	@Autowired
	private PermissionService permissionService;

	@ResponseBody
	@RequestMapping("/list")
	public List<Permission> findList() {
		return permissionService.findList();
	}

	@ResponseBody
	@RequestMapping("/tree")
	public List<TreeNode> getRoleResourceTree() {
		return permissionService.getTree();
	}
}
