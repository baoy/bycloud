package org.bycloud.security.realm;

import java.util.Set;

import org.apache.shiro.SecurityUtils;
import org.bycloud.api.auth.UserService;
import org.bycloud.common.model.User;
import org.bycloud.common.util.SpringUtils;

public class UserUtils {

	public static UserService userService = SpringUtils.getBean(UserService.class);

	public static String getUsername() {
		return (String) SecurityUtils.getSubject().getPrincipal();
	}

	public static User getCurrentUser() {
		return userService.findByUsername(getUsername());
	}

	public static Set<String> getUserPermission() {
		return userService.findPermissions(getUsername());
	}
}
