package org.bycloud.api.auth;

import java.util.List;
import java.util.Set;

import org.bycloud.common.TreeNode;
import org.bycloud.common.model.Resource;

public interface ResourceService {

	public List<Resource> findChildren(String node);

	public List<TreeNode> findTreeNodes(String node, Boolean checked);

	public List<TreeNode> getTree();

	public List<Resource> findAll();

	public boolean save(Resource resource);

	public Set<String> findPermissionIds(String resourceId);
}
