package org.bycloud.security.realm;

import java.io.PrintWriter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;
import org.bycloud.common.Constants;
import org.bycloud.common.Result;
import org.bycloud.common.util.CookieUtil;
import org.bycloud.common.util.RedisPoolUtils;
import org.bycloud.common.util.RequestUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

@Service("formAuthenticationFilter")
public class FormAuthenticationFilter extends org.apache.shiro.web.filter.authc.FormAuthenticationFilter {

    String captchaParam = Constants.CAPTCHA;

    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
        String username = getUsername(request);
        String password = getPassword(request);
        boolean rememberMe = isRememberMe(request);
        String host = RequestUtils.getRemoteAddr((HttpServletRequest) request);
        String captcha = org.apache.shiro.web.util.WebUtils.getCleanParam(request, captchaParam);
        return new UsernamePasswordToken(username, password.toCharArray(), rememberMe, host, captcha);
    }

    /**
     * 验证码校验, 调试阶段注释代码
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        // HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        // String sessionCaptcha = (String)
        // org.springframework.web.util.WebUtils.getSessionAttribute(httpServletRequest,
        // captchaParam);
        // String requestCaptcha =
        // org.apache.shiro.web.util.WebUtils.getCleanParam(request, captchaParam);
        // if (!StringUtils.endsWithIgnoreCase(sessionCaptcha, requestCaptcha)) {
        // httpServletRequest.setAttribute(DEFAULT_ERROR_KEY_ATTRIBUTE_NAME, "验证码错误!");
        // return true;
        // }
        return super.onAccessDenied(request, response);
    }

    @Override
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject,
                                     ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest req = WebUtils.toHttp(request);
        HttpServletResponse resp = WebUtils.toHttp(response);
        if (!"XMLHttpRequest".equalsIgnoreCase(req.getHeader("X-Requested-With"))) {
            issueSuccessRedirect(request, response);
        } else {
            resp.setCharacterEncoding("UTF-8");
            PrintWriter out = resp.getWriter();
            out.write(JSONObject.toJSONString(Result.writeSuccessData("/index")));
            out.flush();
            out.close();
        }
        return false;
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        return super.isAccessAllowed(request, response, mappedValue);
    }

}