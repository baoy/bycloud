package org.bycloud.common;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Pager<T> {

    int total = 0;
    List<T> data;

    public static <T> Pager<T> getResult(int total, List<T> data) {
        return new Pager<T>(total, data);
    }

}
