Array.prototype.contains = function(obj) {
	var self = this, ret = true;
	if (obj instanceof Array) { // 数组
		loop: for (var i = 0; i < obj.length; i++) {
			for (var j = 0; j < self.length; j++) {
				if (self[j] === obj[i]) {
					continue loop;
				}
			}
			ret = false;
		}
	} else { // 对象
		var i = this.length, ret = false;
		while (i--) {
			if (self[i] === obj) {
				ret = true;
			}
		}
	}
	return ret;
};


function getTopWindow() {
	var topWin = window;
	try {
		while (topWin.parent && topWin.parent != topWin && topWin.parent.location.href.indexOf('wrapper') == -1) {
			topWin = topWin.parent
		}
	} catch (e) {
	}
	return topWin;
}

function getxhr() {
	var _xhr = false;
	try {
		_xhr = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			_xhr = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e2) {
			_xhr = false;
		}
	}
	if (!_xhr && window.XMLHttpRequest)
		_xhr = new XMLHttpRequest();
	return _xhr;
}

function http_get(url) {
	var xhr = getxhr();
	url.indexOf('?') == -1 ? url += '?' : 0
	url += '&_ts=' + new Date().getTime()
	xhr.open('GET', url, false);
	xhr.send();
	if (xhr.status != 200){
		if (window.console){
			console.log('http_get-error','status:'+xhr.status,xhr.responseText)
		}
		return ''
	}
	return xhr.responseText;
}

function LoginUser() {

	this.filterAllowed = function(objlist, authNamePath) {
		authNamePath = authNamePath || 'authName'
		var pers = window['check_per_ret'] || [];
		if (objlist && objlist instanceof Array) {
			loop1: for (var k = 0; k < objlist.length; k++) {
				var needPermission = objlist[k][authNamePath];
				if (!needPermission) {
					continue loop1;
				}
				loop2: for (var i = 0; i < pers.length; i++) {
					var p1 = new WildcardPermission(pers[i].toLowerCase());
					var p2 = new WildcardPermission(needPermission.toLowerCase());
					if (p1.implies(p2)) {
						continue loop1;
					}
				}
				objlist.splice(k, 1);
				k = k - 1;
			}
		}
		return objlist;
	}
}

