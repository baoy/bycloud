Ext.define('Baoy.Application', {
    appFolder: 'admin',
    extend: 'Ext.app.Application',
    name: 'Baoy',
    init: function () {
        Ext.setGlyphFontFamily('FontAwesome');
        Ext.log('Application init......');
        // 设置button menu 的时候在console中显示错误，加了这句就好了。
        Ext.enableAriaButtons = false;

        // 如果一个 panel 没有设置title，会在console里面显示一个警告信息，加上这个就没了
        Ext.enableAriaPanels = false;
        //禁止整个页面的右键
//		Ext.getDoc().on("contextmenu", function(e) {
//			e.stopEvent();
//		});
    },

    // 主界面加载好后执行这里的 launch 函数
    launch: function () {
    },
    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update',
            'This application has an update, reload?', function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            });
    }
});

Ext.apply(Ext, {
    // 应用显示名称
    appName: "WEB快速开发平台-bycloud",
    // 提示信息
    info: function (msg) {
        Ext.toast({
            html: "<font color='green'>" + msg + "!</font>",
            title: '消息',
            width: 300,
            align: 't'
        });
    },
    // 错误消息
    error: function (msg) {
        Ext.toast({
            html: "<font color='red'>" + msg + "!</font>",
            title: '错误',
            width: 300,
            align: 't'
        });
    },
    // 登录用户名
    getLoginUser: function () {
        return window['user_login'] || '';
    }
});