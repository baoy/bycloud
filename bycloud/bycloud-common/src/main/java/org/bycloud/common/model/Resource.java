package org.bycloud.common.model;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Setter
@Getter
@ToString
public class Resource implements Serializable {

    private static final long serialVersionUID = -5850569474755807221L;
    private String id; // 编号
    private String name; // 资源名称
    private String type = ResourceType.menu.name(); // 资源类型
    private String url; // 资源路径
    private String permission; // 权限字符串
    private String parentId; // 父编号
    private String parentIds; // 父编号列表
    private Boolean available = Boolean.FALSE;
    
    //vo 
    Boolean checked;
    String permissionId;
    String resourceId;
    Boolean leaf = false;
    List<String> permissionIds;
   

    public static enum ResourceType {
        menu("菜单"), button("按钮");

        private final String info;

        private ResourceType(String info) {
            this.info = info;
        }

        public String getInfo() {
            return info;
        }
    }
    
    public boolean isRootNode() {
        return  StringUtils.equals(this.parentId, "0");
    }
}
