package org.bycloud.security.realm;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.session.SessionException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.bycloud.common.Constants;
import org.bycloud.common.util.CookieUtil;
import org.springframework.stereotype.Component;

@Component
public class CustomLogoutFilter extends LogoutFilter {
	@Override
	protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
		Subject subject = getSubject(request, response);
		String redirectUrl = getRedirectUrl(request, response, subject);
		ServletContext context = request.getServletContext();

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		CookieUtil.delLoginToken(req, resp);
		req.getSession().removeAttribute(Constants.CURRENT_USER);
		try {
			subject.logout();
			context.removeAttribute("error");
		} catch (SessionException e) {
			e.printStackTrace();
		}
		issueRedirect(request, response, redirectUrl);
		return false;
	}
}
