Ext.define('Baoy.controller.dashboard.MainController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.dashboard-main-view',

	onTabChange : function(tabs, newTab, oldTab) {
		Ext.suspendLayouts();
		//				newTab.setTitle('Active Tab');
		//				oldTab.setTitle('Inactive Tab');
		Ext.resumeLayouts(true);
	},

	onModifyPwd : function() {
		var topView = this.getView();
		if (!topView.pwdWin) {
			var pwdWin = topView.pwdWin = Ext.create('Ext.window.Window', {
				width : 500,
				closeAction : 'method-hide',
				title : '修改密码',
				modal : true,
				items : [ {
					xtype : 'form',
					url : 'user/pwd',
					defaults : {
						labelWidth : 60,
						xtype : 'textfield',
						inputType : 'password',
						anchor : '100%',
						allowBlank : false
					},
					bodyPadding : 10,
					items : [ {
						fieldLabel : '新密码',
						name : 'password',
						allowBlank : false
					} ]
				} ],
				buttons : [ {
					text : '修改',
					handler : function() {
						var pwdForm = pwdWin.down('form').getForm();
						if (pwdForm.isValid()) {
							pwdForm.submit({
								success : function(form, action) {
									if (action.result.success) {
										pwdWin.hide();
									}
									Ext.info(action.result.msg);
								},
								failure : function(form, action) {
									Ext.error(action.result.msg);
								}
							})
						}
					}
				}, {
					text : '重置',
					handler : function(btn, e) {
						pwdWin.down('form').getForm().reset();
					}
				} ]
			});
		}
		topView.pwdWin.show();
	},

	onLogout : function() {
		Ext.MessageBox.confirm("警告", "是否退出系统？", function(btn) {
			if (btn == 'yes') {
				getTopWindow().location.href = '/logout';
			}
		});
	}
})