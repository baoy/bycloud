Ext.define('Baoy.controller.system.user.SystemUserController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.system-user-controller',

	onAddUser : function(btn, e) {
		var grid = this.getView();
		if (!grid.editUserWin)
			grid.editUserWin = Ext
					.create('Baoy.view.system.user.SystemUserEditView');
		grid.editUserWin.getViewModel().set('windowTitle', '新增用户');
		var dataForm = grid.editUserWin.down('form').getForm();
		dataForm.reset();
		grid.editUserWin.show();
	},

	onEditUser : function(btn, e) {
		var grid = this.getView();
		var selectRows = grid.getSelection();
		if (selectRows && selectRows.length > 0) {
			selectRow = selectRows[0];
			if (!grid.editUserWin) {
				var editUserWin = Ext.create('Baoy.view.system.user.SystemUserEditView');
				editUserWin.parentGrid = grid;
				grid.editUserWin = editUserWin;
			}
			var roles = http_get('user/roles?userId=' + selectRow.data.id);
			if (roles) {
				var rs = Ext.decode(roles);
				selectRow.data.roleIds = rs;
			}
			grid.editUserWin.getViewModel().set('windowTitle', '修改用户');
			var dataForm = grid.editUserWin.down('form').getForm();
			dataForm.reset();
			dataForm.loadRecord(selectRow);
			grid.editUserWin.show();
		}
	},

	onRemoveUser : function(btn, e) {
		Ext.MessageBox.confirm('Confirm', 'Are you sure you want to do that?',
				function(optional) {
					if (optional == 'yes') {
						var grid = this.getView();
						grid.getStore().remove(grid.getSelection());
						grid.getView().refresh();
					}
				}, this);
	},

	onRowDblClick : function(me, record, element, rowIndex, e, eOpts) {
		this.onEditUser()
	},

	onSaveUser : function(btn, e) {
		var self = this;
		var form = this.getView().down('form').getForm();
		if (form.isValid()) {
			form.submit({
				success : function(form, action) {
					if (action.result.success) {
						self.getView().hide();
					}
					Ext.info(action.result.msg);
				},
				failure : function(form, action) {
					Ext.Msg.alert('Failed', action.result.msg);
				}
			});
		}
	},

	onHideRefresh : function() {
		var editWin = this.getView();
		editWin.parentGrid.getStore().reload();
	}
})